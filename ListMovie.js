import React from 'react'
import { StyleSheet, View, Text, Image, Dimensions, TouchableOpacity } from 'react-native'

let net = require('react-native-tcp');

const window = Dimensions.get("window");

class ListMovie extends React.Component {

  constructor(props) {
      super(props);

      if (props.data.type === 0) { // FILM
          this.state = { title: "wait ...", type: 0, movie_index: props.data.movie_index };
      }
      else if (props.data.type === 1) { // SERIE
          this.state = { title: "wait ...", type: 1, serie_index: props.data.serie_index };
      }

  }

  componentDidMount() {

    if (this.state.type === 0) { // FILM
      let client = net.createConnection(8888, '192.168.1.19', () => {
           client.write('get_info_movie(' + this.state.movie_index + ')');
      });

      client.on('data', (data) => {
          this.setState({title: data});
      });
    }
    else if (this.state.type === 1) { // SERIE
      let client = net.createConnection(8888, '192.168.1.19', () => {
           client.write('get_info_serie_name(' + this.state.serie_index + ')');
      });

      client.on('data', (data) => {
          this.setState({title: data});
      });
    }
  }

  render() {

    let title = this.state.title + "";

    let touchable = null;

    if (this.state.type === 0) {

      touchable = (
        <TouchableOpacity activeOpacity={0.9} onPress={() => this.props.action_press_list(this.state.type, this.state.movie_index)} style={styles.main_container}>
          <Text style={styles.text}>{title}</Text>
          <Image
            source={{ uri: 'https://reactnativecode.com/wp-content/uploads/2019/02/arrow_right_icon.png' }}
            style={styles.iconStyle}
          />
        </TouchableOpacity>
      );

    }
    else {

      touchable = (
        <TouchableOpacity activeOpacity={0.9}  onPress={() => this.props.action_press_list(this.state.type, this.state.serie_index)}  style={styles.main_container}>
          <Text style={styles.text}>{title}</Text>
          <Image
            source={{ uri: 'https://reactnativecode.com/wp-content/uploads/2019/02/arrow_right_icon.png' }}
            style={styles.iconStyle}
          />
        </TouchableOpacity>
      );

    }

    return (
      <React.Fragment>
        {touchable}
      </React.Fragment>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    width: window.width,
    height: 50,
    backgroundColor: '#fcfcfc',
    flex: 1,
//    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  text: {
    marginLeft: 20,
    marginRight: 20,
    flex: 8
  },
  iconStyle:  {
    flex: 1
  },
})

export default ListMovie
