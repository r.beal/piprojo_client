/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    Image,
    Button,
    Alert,
    FlatList,
    Dimensions,
} from 'react-native';

import ListMovie from './ListMovie'

const window = Dimensions.get("window");


let net = require('react-native-tcp');
let film_list = []

export default class PiProjo extends Component {

    constructor(props) {
        super(props);

        this.state = { render_switch: 1, nb_movies: 0, list_film: [] };

        this.try_connect = this.try_connect.bind(this);

        this.scan = this.scan.bind(this);
        this.action_press_list = this.action_press_list.bind(this)
    }

    action_press_list(type, id) {

        if (type === 0) {
          console.debug("film : " + id)
        }
        if (type === 1) {
          console.debug("serie : " + id)
        }
    }



  try_connect() {
    let client = net.createConnection(8888, '192.168.1.19', () => {
        client.write('ping()');
    });

    client.on('data', (data) => {
        if (data == 'OK') {

            this.scan();
        }
    });
  }

  scan() {
    let client = net.createConnection(8888, '192.168.1.19', () => {
        client.write('scan()');
    });

    client.on('data', (data) => {
        //this.setState({nb_movies: data});
        console.debug("nb movies :" + data)
        let ret_scan = JSON.parse(data);

        console.debug("nb films=" + ret_scan["movies"])

        for (let i=0; i< ret_scan["movies"]; i++) {
          film_list.push({id: Math.random().toString(), type: 0, movie_index: i })
        }

        for (let i=0; i< ret_scan["series"]; i++)
          film_list.push({id: Math.random().toString(), type: 1, serie_index: i })



        this.setState({render_switch: 1});

    });
  }

  render() {
    let to_render = null;

    if (this.state.render_switch === 0) { // FIRST PAGE

        to_render = (
            <React.Fragment>
                <Image source={require('./img/poule.png')} style={styles.poule}/>
                <View style={styles.button}>
                    <Button onPress={this.try_connect} title="Connect to piProjo"/>
                </View>
            </React.Fragment>
        );
    }

    else if (this.state.render_switch === 1) {
      to_render = (
            <View style={styles_home.vcontainer}>
              <View style={styles_home.vvolume}>
                <View style={styles_home.vvolume_left}>
                  <View style={styles_home.volume_left}>
                  </View>
                  <View style={styles_home.volume_lright}>
                  </View>
                </View>
                <View style={styles_home.vvolume_right}>
                  <Text style={styles_home.text_vol_iface}>HDMI</Text>
                  <View style={styles_home.volume_right_sep}/>
                  <Text style={styles_home.text_vol_iface}>JACK</Text>
                </View>
              </View>
              <View style={styles_home.vbrowse}>
                <Text style={styles_home.browse_text}>BROWSE</Text>

              </View>
              <View style={styles_home.vimg}>
                <Image
                  style={styles_home.img}
                  source={require('./img/ali.jpg')}
                />
              </View>
              <View style={styles_home.vinfo}>
                <View style={styles_home.info_left}>
                  <Text style={styles_home.info_left_title}>-- Ali G --</Text>
                  <Text style={styles_home.info_left_inginfo}>19200kbps - full HD comme ta soeur</Text>
                </View>
                <View style={styles_home.info_right}>
                  <Image
                    style={styles_home.info_right_img}
                    source={require('./img/sett.png')}
                  />
                </View>
              </View>
              <View style={styles_home.vplayer}>
              <Image
                style={styles_home.player_icon}
                source={require('./img/stop.png')}
              />
              <Image
                style={styles_home.player_icon}
                source={require('./img/play.png')}
              />
              <Image
                style={styles_home.player_icon}
                source={require('./img/next.png')}
              />
              </View>
            </View>
      );
    }

    else if (this.state.render_switch === 2) {

        to_render = (
            <React.Fragment>
                <FlatList
                  data={film_list}
                  renderItem={({item}) => <ListMovie data={item} action_press_list={this.action_press_list} />}
                />
            </React.Fragment>
        );
    }

    return (
        <View style={styles.container}>
            {to_render}
        </View>
    );

  }
}

const styles_home = StyleSheet.create({
  vcontainer: {
    flex: 1,
    backgroundColor: '#2d2d2d',
    flexDirection: 'column'
  },
  // VOLUME
  vvolume: {
    width: window.width,
    height: '100%',
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  vvolume_left: {
    flex: 4,
    height: '30%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  volume_left: {
    height: '100%',
    width: '30%',
    backgroundColor: 'rgb(110, 110, 110)',
    borderBottomLeftRadius: 10,
    borderTopLeftRadius: 10,
  },
  volume_lright: {
    height: '100%',
    width: '60%',
    backgroundColor: 'rgb(203, 203, 203)',
    borderBottomRightRadius: 10,
    borderTopRightRadius: 10,
  },
  vvolume_right: {
    flex: 1,
    height: '30%',
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  volume_right_sep: {
    height: 1,
    width: '80%',
    backgroundColor: 'rgb(203, 203, 203)',
    borderRadius: 10
  },
  text_vol_iface: {
    fontSize: 20,
    color: '#ffffff',
  },
  // /VOLUME

  // BROWSE
  vbrowse: {
    width: window.width,
    height: '100%',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2562b6',
  },
  browse_text: {
    fontSize: 50,
  },
  // /BROWSE

  // IMG
  vimg: {
    width: window.width,
    flex: 4,
  },
  img: {
    height: '100%',
  },
  // /INFO
  vinfo: {
    width: window.width,
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#b6b6b6',
  },
  info_left: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  info_right: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',

  },
  info_left_title: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  info_left_inginfo: {
    fontSize: 20,

  },
  info_right_img: {
    height: '60%',
    width: undefined,
    aspectRatio: 1,
  },
  // /INFO

  // PLAYER
  vplayer: {
    width: window.width,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  player_icon: {
    height: '80%',
    width: undefined,
    aspectRatio: 1,
  },
  // /PLAYER
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2d2d2d',
    alignItems: 'center',
    justifyContent: 'center',
  },
  poule: {
    flex: 4,
    width: '90%',
    resizeMode: 'contain',
    justifyContent: 'center',
  },
  button: {
    flex: 1,
    width: '90%'
  },
});
